#ifndef timer_control
#define timer_control

#include "Arduino.h"


class timerControl
{
public:
  timerControl(long interval_time); // set interval delay
  boolean check();
  void updateInterval(long interval_update); // update interval period
  long getInterval(); // return current interval
private:
  unsigned long _nextMillis; // which pin output is connected to
  unsigned long _interval; // which pin output is connected to
};


#endif
