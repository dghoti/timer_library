#include "Arduino.h"
#include "timer.h"



timerControl::timerControl(long interval_time)
{
  _interval=interval_time;
  _nextMillis=millis()+_interval;
}

boolean timerControl::check()
{
  if( (long)( millis() - _nextMillis ) >= 0)
  {
    _nextMillis+=_interval;
    return true;
  }
}else{
  return false;
}

void timerControl::updateInterval(long interval_update)
{
  _interval=interval_time;
  _nextMillis=millis()+_interval;
}

long timerControl::getInterval()
{
  return _interval;
}
